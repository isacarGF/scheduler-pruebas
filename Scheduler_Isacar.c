#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

typedef struct dato

{

    char *cadena;
    int x,y;
} paramentro;

 void gotoxy (int x, int y){
         printf("\033[%d;%df", y,x);

}

void *imprimir(void *args){
    parametro *par=(parametro *)args;
    int i;
    for(i=0;i < strlen(par->cadena);i++){
        fflush(stdout);
        gotoxy(par->x,par->y);
        par->x++;
        printf("%c",par->cadena[i]);
        sleep(1);
     }
     printf("\n");
}

int main(int argc, char const *argv[]){

     pthread t hilo1,hilo2,hilo3,hilo4,hilo5;
     char *cadena="Parametro";
     parametro p1;
     p1.cadena="Procesamiento 1";
     p1.x=10;
     p1.y=20;

     parametro p2;
     p2.cadena="Procesamiento 2";
     p2.x=20;
     p2.y=20;

     parametro p3;
     p3.cadena="Procesamiento 3";
     p3.x=20;
     p3.y=20;

     parametro p4;
     p4.cadena="Procesamiento 4";
     p4.x=20;
     p4.y=20;

     parametro p5;
     p5.cadena="Procesamiento 5";
     p5.x=20;
     p5.y=20;


     pthread_create(&hilo1,NULL,imprimir,(void *)&p1);

     pthread_create(&hilo2,NULL,imprimir,(void *)&p2);

     pthread_create(&hilo3,NULL,imprimir,(void *)&p3);

     pthread_create(&hilo4,NULL,imprimir,(void *)&p4);

     pthread_create(&hilo5,NULL,imprimir,(void *)&p5);



     pthread_join(hilo1,NULL);
     pthread_join(hilo2,NULL);
     pthread_join(hilo3,NULL);
     pthread_join(hilo4,NULL);
     pthread_join(hilo5,NULL);


     return 0;

}